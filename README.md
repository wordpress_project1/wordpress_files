# Использованные пакеты и зависимости

- **nginx** - веб-сервер, где будет крутиться наш сайт на Wordpress.
- **php-fpm** - менеджер процессов PHP FastCGI, который позволяет веб-серверу обрабатывать большее количество запросов одновременно.
- **php-mysql** - модуль БД для php-приложений.
- **apache2-utils** - дополнительные утилиты Apache, в нашем случае будет использоваться для модуля `htpasswd`.
- **mysql-client** - клиентская часть БД MySQL.

---

# Изменён конфиг файл wp-conig.php


- ** Имя БД ** - `define( 'DB_NAME', 'wordpress_db' );`

- **Имя пользователя БД** - `define( 'DB_USER', 'wp_user' );`

- **Пароль от БД** - `define( 'DB_PASSWORD', 'password' );`

- **IP адрес сервера БД** - `define( 'DB_HOST', '10.182.10.230' );`

---

# Конфиг файл nginx 

```nginx
server {

    listen 80;
    server_name web.uberlegenheit.ru;

    root /home/web;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.html index.htm index.php;

    auth_basic      "Protected";
    auth_basic_user_file /etc/nginx/.htpasswd;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }
}
```

